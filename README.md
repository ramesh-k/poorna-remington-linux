This is an extended remington keyboard which includes all malayalam charecters.
# install
clone the repositoty\
copy ml-poornaremington.mim to `/usr/share/m17n`\
Add the layout to your Ibus preferences.\
it needed ibus and m17n packages in your computer. detailed installation steps can be found [Here](https://wiki.smc.org.in/Remington)

# Use
After installation it can be used by adding Poorna Extended remington from ibus.\
The layout is a four layer one, that is a single key maps to a maximum of 4 characters. For example,
in case of 'd',\
'd' gives ക\
'Shift+d' gives ഖ\
'Right Alt+d' gives ഽ\
'Right Alt+Shift+d' gives ഁ

# Layout Image
![Image](poorna_remington.png)
